<?php
trait Hewan{
    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo"{$this->nama} Sedang {$this->keahlian}";
    }
}


abstract class Fight{
    use Hewan;
    public $attackPower;
    public $deffencePower;

    public function serang($hewan){
        echo"{$this->nama} sedang menyerang {$hewan->nama}";
        $hewan->diserang($this);
    }
    public function diserang($hewan){
        echo"{$this->nama} sedang diserang {$hewan->nama}";
        $this->darah = $this->darah-$hewan->attackPower/$this->$hewan->deffencePower;
        
    }
    protected function getInfo(){
        echo"<br>";
        echo"nama: {$this->nama}";
        echo"<br>";
        echo"darah: {$this->darah}";
        echo"<br>";
        echo"jumlahKaki: {$this->jumlahKaki}";
        echo"<br>";
        echo"Attackpower: {$this->attackPower}";
        echo"<br>";
        echo"deffencePower: {$this->deffencePower}";

        $this->atraksi();
    }
    abstract public function getInfoHewan();
}
class Elang extends Fight{
    public function __construct($nama){
        $this->nama=$nama;
        $this->jumlahKaki=2;
        $this->keahlian="terbang tinggi";
        $this->attackPower=10;
        $this->deffencePower=5;
    }
    public function getInfoHewan(){
        echo"jenis hewan: Elang";
    }
}
class Harimau extends Fight{
    public function __construct($nama){
        $this->nama=$nama;
        $this->jumlahKaki="2";
        $this->keahlian="lari cepat";
        $this->attackPower=7;
        $this->deffencePower=8;
    }
    public function getInfoHewan(){
        echo"jenis hewan: Harimau";
    }
}
class baris{
    public static function tampilBaris(){
        echo"<br>";
        echo"============";
        echo"<br>";
    }
}
$elang=new Elang("Elang");
$elang->getInfoHewan();
baris::tampilBaris();
$harimau=new Harimau("Harimau");
$harimau->getInfoHewan();
baris::tampilBaris();
$elang->serang($harimau);
baris::tampilBaris();
$harimau->getInfoHewan();
baris::tampilBaris();
$elang->getInfoHewan();
?>