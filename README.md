# Laravel Vue Crowdfunding Website Batch 34


## Screenshot

<p style="text-align: center;">
  <img src="crowdfunding-website/screenshot/Screenshot1.png" width="550" style="border: 2px solid #000">
  <img src="crowdfunding-website/screenshot/Screenshot2.png" width="550" style="border: 2px solid #000">
  <img src="crowdfunding-website/screenshot/Screenshot3.png" width="550" style="border: 2px solid #000">
  <img src="crowdfunding-website/screenshot/Screenshot4.png" width="550" style="border: 2px solid #000">
  <img src="crowdfunding-website/screenshot/Screenshot5.png" width="550" style="border: 2px solid #000">
</p>

## Application Demo
[APPLICATION DEMO](https://youtu.be/H-fy25VJ9pc)