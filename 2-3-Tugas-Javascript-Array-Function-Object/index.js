// Soal 1
console.log("Nomor 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();
console.info(daftarHewan);

// Soal 2
console.log("Nomor 2");
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

function introduce(data){
    return `Nama saya ${data.name} , umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
}

// Soal 3
console.log("Nomor 3");
var hitung1 = "Muhammad"
const regex=/[aiueo]/ig;

const tu=[];
while((result=regex.exec(hitung1)) !==null){
    
    tu.push(result);
    
}

var hitung2 = "Iqbal";
const tu2=[];
while((result=regex.exec(hitung2)) !==null){
    
    tu2.push(result);
    
}
console.log(tu.length,tu2.length);

// soal 4
console.log("Nomor 4")
function hitung(angka){
    return angka*2-2;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));