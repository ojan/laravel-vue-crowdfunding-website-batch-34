<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('app');
// });
// Route::get('/{any?}', function () {
//     return 'masuk kesini';
// })->where('any', '.*');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/route-1', function(){
//     return 'masuk ke route-1 email sudah terverifikasi';
// })->middleware(['auth','email_verified']);
// Route::get('/route-2', function(){
//     return 'masuk ke route-2 email sudah terverifikasi dan anda admin';
// })->middleware(['auth','email_verified','admin']);

Route::view('/{any?}', 'app')->where('any', '.*');
