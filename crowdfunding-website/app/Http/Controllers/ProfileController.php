<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
class ProfileController extends Controller
{
    public function getProfile(Request $request)
    {
        $data['user']=auth()->user();
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'Profile Showed',
            'data'=>$data
        ],200);
        
    }
    public function updateProfile(Request $request)
    {
        // $request->validate([
        //     'name'=>'required',
        //     'photo_profile'=>'required|image|mimes:jpeg,jpg,png'
        // ]);
        $user=auth()->user();
        if($request->hasFile('photo_profile')){
        $photo_profile=$request->file('photo_profile');
        $photo_profile_extension = $photo_profile->getClientOriginalExtension();
        $photo_profile_name = Str::slug($user->name,'-').'-'.$user->id.".".$photo_profile_extension;  
        $photo_profile_folder='/images/profile/';
        $photo_profile_location=$photo_profile_folder.$photo_profile_name;
        try{
            $photo_profile->move(public_path($photo_profile_folder),$photo_profile_name);
            $user->update([
                'photo_profile'=>$photo_profile_location,
            ]);
        }catch(\Throwable $th){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Photo Profile gagal diupload',
                'data'=>$user
            ],400);
        }
    }
        $user->update([
            'name'=>$request->name,
        ]);
        
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'Profile Updated',
            'data'=>$user
        ],200);
    }
}
