<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
// use Mail;
// use App\Mail\UserRegisteredMail;
use App\Events\UserRegisterEvent;
class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email'=>'required|unique:users,email|email',
            'username'=>'required|unique:users,username',
            'name'=>'required',
        ]);
        $data_request=$request->all();
        $user = User::create($data_request);
        event(new UserRegisterEvent($user));
        $data['user']=$user;
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'user berhasil di daftarkan, silahkan check email untuk kode otp',
            'data'=>$data
        ]);
    }
}
