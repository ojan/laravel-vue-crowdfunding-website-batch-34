<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\otp_code;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp'=>'required'
        ]);
        $otp_code = otp_code::where('otp',$request->otp)->first();
        if(!$otp_code){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'otp code tidak ditemukan',
            ],400);
        }
        $now=Carbon::now()->timezone('Asia/Jakarta');
        if($now>$otp_code->valid_until){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'otp code sudah tidak berlaku, silahkan generate ulang',
            ],400);
        }
        $user=User::find($otp_code->user_id);
        $user->email_verified_at = $now;
        $user->save();
        $otp_code->delete();
        $data['user']=$user;
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'user berhasil di verifikasi',
            'data'=>$data,
        ],200);
    }
}
