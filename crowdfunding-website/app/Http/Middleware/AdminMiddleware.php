<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user= auth()->user();
        if($user->role_id == 'd8d96b89-41ab-4068-993a-b9d973fcd831'){
            return $next($request);
        }
        return response()->json([
            'message'=>'Role anda bukan admin'
        ]);
        
    }
}
