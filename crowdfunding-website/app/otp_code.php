<?php

namespace App;
use App\Traits\UseUUID;
use Illuminate\Database\Eloquent\Model;

class otp_code extends Model
{
    use UseUUID;
    protected $fillable=['otp','valid_until','user_id'];   

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
