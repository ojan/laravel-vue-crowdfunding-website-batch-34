<?php

namespace App;
use App\Traits\UseUUID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $fillable=['name'];
    protected $primaryKey='id';
     use UseUUID;

    public function user(){
        return $this->hasMany('App\User','role_id');
    }
    
}
