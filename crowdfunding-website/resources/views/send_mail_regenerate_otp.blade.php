<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body style="font-family: sans-serif;">
    <div style="display: block; margin: auto; max-width: 600px;" class="main">
        <h1 style="font-size: 18px; font-weight: bold; margin-top: 20px">Selamat Bapak/Ibu {{ $name }} OTP Telah
            di Perbaharui
        </h1>
        <p>Berikut adalah OTP Code baru anda:</p>
        <div style="background-color: orange;">
            <h3 style="color:white; text-align:center;">{{ $otp }}</h3>
        </div>
        <p>Gunakan Sebelum 5 menit dari Sekarang</p>
    </div>
    <!-- Example of invalid for email html/css, will be detected by Mailtrap: -->
    <style>
        .main {
            background-color: white;
        }

        a:hover {
            border-left-width: 1em;
            min-height: 2em;
        }
    </style>
</body>

</html>
